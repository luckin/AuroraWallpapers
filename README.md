<img src="https://www.gnu.org/graphics/gplv3-88x31.png" alt="GPL v3 Logo">

# Aurora Wallpapers: A Wallhaven Client

*Aurora Wallpapers* is an unofficial, FOSS client for Wallhaven. 

# Features

1. Free/Libre software - Has GPLv3 License
2. Beautiful design - Based on latest material design guidelines
3. Great collection of wallpapers

# Screenshots

<img src="https://gitlab.com/AuroraOSS/AuroraWallpapers/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/ss001.png" height="400"><img src="https://gitlab.com/AuroraOSS/AuroraWallpapers/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/ss002.png" height="400">
<img src="https://gitlab.com/AuroraOSS/AuroraWallpapers/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/ss003.png" height="400"><img src="https://gitlab.com/AuroraOSS/AuroraWallpapers/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/ss004.png" height="400">
<img src="https://gitlab.com/AuroraOSS/AuroraWallpapers/raw/master/fastlane/metadata/android/en-US/images/phoneScreenshots/ss005.png" height="400">


# Support Developement

<img src="https://img.shields.io/static/v1?label=Bitcoin&message=bc1qu7cy9fepjj309y4r2x3rymve7mw4ff39c8cpe0&color=Orange">

<img src="https://img.shields.io/static/v1?label=Bitcoin Cash&message=qpqus3qdlz8guf476vwz0fjl8s34fseukcmrl6eknl&color=Success">

<img src="https://img.shields.io/static/v1?label=Ethereum&message=0x6977446933EC8b5964D921f7377950992337B1C6&color=Blue">

<img src="https://img.shields.io/static/v1?label=BHIM UPI&message=whyorean@dbs&color=BlueViolet">

* Paypal - [Link](https://paypal.me/AuroraDev)
* LiberaPay - [Link](https://liberapay.com/on/gitlab/whyorean/)
  
# Links
* AndroidFileHost - [Downloads](https://androidfilehost.com/?w=files&flid=294487)
* XDA Labs - [Link](https://labs.xda-developers.com/store/app/com.aurora.wallpapers)
* Support Group - [Telegram](https://t.me/AuroraOfficial)

# Aurora Wallpaper uses the following major Open Source libraries:

* [RX-Java](https://github.com/ReactiveX/RxJava)
* [ButterKnife](https://github.com/JakeWharton/butterknife)
* [Fastadapter](https://github.com/mikepenz/FastAdapter)
* [Retrofit](https://square.github.io/retrofit/)
* [Glide](https://github.com/bumptech/glide)
* [BigImageViewer](https://github.com/Piasy/BigImageViewer)

