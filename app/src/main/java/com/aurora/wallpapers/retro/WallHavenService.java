/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.retro;

import com.aurora.wallpapers.model.wallhaven.Info;
import com.aurora.wallpapers.model.wallhaven.Search;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface WallHavenService {
    @GET("search")
    Call<Search> getEmptySearch();

    @GET("search")
    Call<Search> searchWalls(@Query("q") String q);

    @GET("w/{id}")
    Call<Info> getWallInfo(@Path("id") String id, @Query("apikey") String apikey);

    @GET("search")
    Call<Search> searchWalls(
            @Query("apikey") String apikey,
            @Query("q") String q,
            @Query("ratios") String ratios,
            @Query("resolutions") String resolutions,
            @Query("purity") String purity,
            @Query("categories") String categories,
            @Query("page") int page
    );

    @GET("search")
    Call<Search> getWalls(
            @Query("apikey") String apikey,
            @Query("sorting") String sorting,
            @Query("ratios") String ratios,
            @Query("resolutions") String resolutions,
            @Query("purity") String purity,
            @Query("categories") String categories,
            @Query("page") int page,
            @Query("seed") String seed,
            @Query("topRange") String topRange
    );

    @GET
    Call<ResponseBody> downloadWallpaper(@Url String fileUrl);
}
