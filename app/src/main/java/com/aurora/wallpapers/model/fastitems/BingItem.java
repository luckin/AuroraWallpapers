/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.model.fastitems;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.ColorUtils;

import com.aurora.wallpapers.GlideApp;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.bing.BingWall;
import com.aurora.wallpapers.utils.Util;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BingItem extends AbstractItem<BingItem.ViewHolder> {

    private BingWall wall;

    public BingItem(BingWall wall) {
        this.wall = wall;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_wall_bing;
    }

    @NotNull
    @Override
    public ViewHolder getViewHolder(@NotNull View view) {
        final int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        view.getLayoutParams().height = height / 4;
        return new ViewHolder(view);
    }

    @Override
    public int getType() {
        return R.id.fastadapter_item;
    }

    public static class ViewHolder extends FastAdapter.ViewHolder<BingItem> {
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.line1)
        TextView line1;
        @BindView(R.id.line2)
        TextView line2;
        @BindView(R.id.layout_bottom)
        RelativeLayout relativeLayout;

        private Context context;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.context = itemView.getContext();
        }

        @Override
        public void bindView(@NotNull BingItem item, @NotNull List<?> list) {
            final BingWall wall = item.getWall();

            line1.setText(wall.getTitle());
            line2.setText(Util.getBingDate(wall.getStartdate()));

            GlideApp.with(context)
                    .asBitmap()
                    .load(Util.getBingUrl(wall.getUrl()))
                    .apply(new RequestOptions().centerCrop())
                    .into(img);

            relativeLayout.setBackgroundColor(ColorUtils.setAlphaComponent(Color.BLACK, 120));
            itemView.setBackgroundColor(Color.BLACK);
            line1.setTextColor(Color.WHITE);
            line2.setTextColor(Color.WHITE);
        }

        @Override
        public void unbindView(@NotNull BingItem item) {
            line1.setText(null);
            line2.setText(null);
            GlideApp.with(context).clear(img);
            img.setImageBitmap(null);
        }
    }
}
