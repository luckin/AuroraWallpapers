/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.model.bing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class BingWall {
    @Expose
    private String startdate;
    @SerializedName("fullstartdate")
    @Expose
    private String fullstartdate;
    @SerializedName("enddate")
    @Expose
    private String enddate;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("urlbase")
    @Expose
    private String urlbase;
    @SerializedName("copyright")
    @Expose
    private String copyright;
    @SerializedName("copyrightlink")
    @Expose
    private String copyrightlink;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("quiz")
    @Expose
    private String quiz;
    @SerializedName("wp")
    @Expose
    private Boolean wp;
    @SerializedName("hsh")
    @Expose
    private String hsh;
    @SerializedName("drk")
    @Expose
    private Integer drk;
    @SerializedName("top")
    @Expose
    private Integer top;
    @SerializedName("bot")
    @Expose
    private Integer bot;
}
