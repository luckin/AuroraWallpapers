/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.utils;

import android.content.Context;
import android.os.Environment;

import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.model.bing.BingWall;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Objects;

import static android.os.Environment.DIRECTORY_PICTURES;

public class FileUtil {

    public static String getFileName(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

    public static String getDefaultDirectory() {
        return Environment.getExternalStoragePublicDirectory(DIRECTORY_PICTURES) + "/Aurora/";
    }

    public static boolean isTempBitmapAvailable(Context context) {
        return new File(StringUtils.joinWith("/", context.getFilesDir(), "temp.png")).exists();
    }

    public static File getTempBitmap(Context context) {
        return new File(StringUtils.joinWith("/", context.getFilesDir(), "temp.png"));
    }

    public static File getDownloadedWallPath(Context context, String id) {
        File baseDirectory = new File(getDefaultDirectory());
        for (File file : Objects.requireNonNull(baseDirectory.listFiles())) {
            if (file.getAbsolutePath().contains(id))
                return file;
        }
        return null;
    }

    public static boolean isWallpaperDownloaded(String id) {
        File baseDirectory = new File(getDefaultDirectory());
        for (File file : Objects.requireNonNull(baseDirectory.listFiles())) {
            if (file.getAbsolutePath().contains(id))
                return true;
        }
        return false;
    }

    public static File getFileFromWallId(Wall wall) {
        final String url = wall.getPath();
        final String fileName = FileUtil.getFileName(url);
        final File fileDirectory = new File(FileUtil.getDefaultDirectory());

        if (!fileDirectory.exists()) {
            fileDirectory.mkdir();
        }

        return new File(StringUtils.joinWith("/", fileDirectory, fileName));
    }

    public static File getFileFromBingWall(BingWall wall) {
        final String fileName = Util.getBingFilename(wall.getHsh());
        final File fileDirectory = new File(FileUtil.getDefaultDirectory());

        if (!fileDirectory.exists()) {
            fileDirectory.mkdir();
        }

        return new File(StringUtils.joinWith("/", fileDirectory, fileName));
    }
}
