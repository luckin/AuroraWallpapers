/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

import com.aurora.wallpapers.service.AutoWallpaperService;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.Util;

public class IntervalReceiver extends BroadcastReceiver {

    static public void setAutoWallpaperInterval(Context context, long interval) {
        if (interval >= 0 && Util.isAutoWallpaperEnabled(context)) {
            PendingIntent pendingIntent = getPendingIntent(context);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                alarmManager.cancel(pendingIntent);
                if (interval > 0) {
                    alarmManager.setRepeating(
                            AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime(),
                            interval,
                            pendingIntent
                    );
                    Log.i("Auto-Wallpaper interval updated to %d ms", interval);
                }
            }
        }
    }

    private static PendingIntent getPendingIntent(Context context) {
        final Intent intent = new Intent(context, IntervalReceiver.class);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Auto-Wallpaper service invoked");
        if (Util.isAutoWallpaperEnabled(context)) {
            Intent autoWallIntent = new Intent(context, AutoWallpaperService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(autoWallIntent);
            } else {
                context.startService(autoWallIntent);
            }
        } else {
            /*Cancel pending intents if auto-wallpaper preference is disabled*/
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null)
                alarmManager.cancel(getPendingIntent(context));
        }
    }
}
