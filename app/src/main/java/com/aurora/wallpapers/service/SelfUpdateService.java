/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;

import com.aurora.wallpapers.BuildConfig;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.retro.RetroClient;
import com.aurora.wallpapers.retro.UpdateService;
import com.aurora.wallpapers.utils.FileUtil;
import com.aurora.wallpapers.utils.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelfUpdateService extends Service {

    private String url;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (intent != null)
            url = intent.getStringExtra(Constants.STRING_EXTRA);
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            url = intent.getStringExtra(Constants.STRING_EXTRA);
            startForeground(1, getNotification());
            startUpdate();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void startUpdate() {
        if (url != null) {

            final String fileName = FileUtil.getFileName(url);
            final File file = new File(getCacheDir(), fileName);

            UpdateService downloadService = RetroClient.getInstance().create(UpdateService.class);
            Call<ResponseBody> call = downloadService.getUpdate(url);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            byte[] fileReader = new byte[4096];
                            InputStream inputStream = response.body().byteStream();
                            OutputStream outputStream = new FileOutputStream(file);

                            while (true) {
                                int read = inputStream.read(fileReader);
                                if (read == -1) {
                                    break;
                                }
                                outputStream.write(fileReader, 0, read);
                            }
                            outputStream.flush();
                            installUpdate(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d(getString(R.string.update_failed));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(SelfUpdateService.this, getString(R.string.update_failed),
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private Notification getNotification() {
        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(this, Constants.NOTIFICATION_CHANNEL_WALLPAPER);
        return builder
                .setAutoCancel(true)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentTitle("Self update")
                .setContentText("Updating in background")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .build();
    }

    private void installUpdate(File file) {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            intent.setData(FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file));
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
