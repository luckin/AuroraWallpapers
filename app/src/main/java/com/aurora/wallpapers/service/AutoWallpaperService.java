/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.manager.FavouritesManager;
import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.task.DailyBingWallsTask;
import com.aurora.wallpapers.task.WallpaperTask;
import com.aurora.wallpapers.task.downloader.BingDownloader;
import com.aurora.wallpapers.task.downloader.WallHavenDownloader;
import com.aurora.wallpapers.utils.FileUtil;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.Util;

import java.io.File;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.SneakyThrows;

public class AutoWallpaperService extends Service {

    private CompositeDisposable disposable = new CompositeDisposable();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @SneakyThrows
    @Override
    public void onCreate() {
        super.onCreate();
        if (Util.isAutoWallpaperEnabled(getApplicationContext())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForeground(1, getNotification());
            } else {
                startForeground(1, new Notification());
            }
            applyWallpaper();
        } else {
            stopSelf();
        }
    }

    private void applyWallpaper() {
        String wallpaperSource = Util.getAutoWallpaperSource(getApplicationContext());
        switch (wallpaperSource) {
            case "0":
                applyDailyBingWallpaper();
                break;
            case "1":
                applyFavouritesWallpaper();
                break;
            case "2":
                applyDownloadedWallpaper();
                break;
        }
    }

    private Notification getNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_WALLPAPER);
        return builder
                .setAutoCancel(true)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setContentTitle("Setting new wallpaper")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .build();
    }

    private void applyDailyBingWallpaper() {
        disposable.add(Observable.fromCallable(() -> new DailyBingWallsTask().getBingWalls(getApplicationContext()))
                .map(bingWalls -> new BingDownloader(bingWalls.get(0)).getWallpaper())
                .map(file -> new WallpaperTask(this, Util.getAutoWallpaperTarget(getApplicationContext())).applyWallpaper(file))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .doOnTerminate(this::stopSelf)
                .subscribe());
    }

    private void applyFavouritesWallpaper() {
        FavouritesManager favouritesManager = new FavouritesManager(getApplicationContext());
        List<Wall> wallList = favouritesManager.getAllWallList();

        if (wallList.isEmpty()) {
            Log.i("No favourite wallpapers available");
            stopSelf();
        } else {
            Wall wall = wallList.get(new Random().nextInt(wallList.size()));
            if (FileUtil.isWallpaperDownloaded(wall.getId())) {
                File file = FileUtil.getDownloadedWallPath(this, wall.getId());
                applyWallpaper(file);
            } else {
                disposable.add(Observable.fromCallable(() -> new WallHavenDownloader(wall).getWallpaper())
                        .map(file -> new WallpaperTask(this, Util.getAutoWallpaperTarget(getApplicationContext())).applyWallpaper(file))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(Throwable::printStackTrace)
                        .doOnTerminate(this::stopSelf)
                        .subscribe());
            }
        }
    }

    private void applyDownloadedWallpaper() {
        File downloadDirectory = new File(FileUtil.getDefaultDirectory());
        File[] files = downloadDirectory.listFiles();

        if (files != null) {
            File file = files[new Random().nextInt(files.length)];
            applyWallpaper(file);
        } else {
            Log.i("No downloaded wallpapers available");
            stopSelf();
        }
    }

    private void applyWallpaper(File file) {
        disposable.add(Observable.fromCallable(() -> new WallpaperTask(this, Util.getAutoWallpaperTarget(getApplicationContext()))
                .applyWallpaper(file))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .doOnTerminate(this::stopSelf)
                .subscribe());
    }

    @Override
    public void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }
}
