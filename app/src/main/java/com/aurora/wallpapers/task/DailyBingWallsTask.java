/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.task;

import android.content.Context;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.model.bing.BingDailyList;
import com.aurora.wallpapers.model.bing.BingWall;
import com.aurora.wallpapers.retro.BingService;
import com.aurora.wallpapers.retro.RetroClient;
import com.aurora.wallpapers.utils.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class DailyBingWallsTask {

    public DailyBingWallsTask() {
    }

    public List<BingWall> getBingWalls(Context context) throws IOException {
        List<BingWall> bingWallList = new ArrayList<>();
        BingService service = RetroClient.getBingInstance().create(BingService.class);
        Call<BingDailyList> call = service.getBingWall(
                Constants.BING_BASE_URL,
                Constants.FILE_JSON,
                0,
                10,
                Util.isCustomBingEnabled(context) ? Util.getCustomBingRegion(context) : "en-US"
        );

        Response<BingDailyList> response = call.execute();
        if (response.body() != null) {
            bingWallList.addAll(response.body().getImages());
        }
        return bingWallList;
    }
}
