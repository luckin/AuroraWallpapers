/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.task;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.aurora.wallpapers.event.Event;
import com.aurora.wallpapers.model.DisplayMetric;
import com.aurora.wallpapers.utils.Util;

import java.io.File;

import glimpse.core.BitmapUtils;
import kotlin.Pair;

public class WallpaperTask {

    private Context context;
    private File file;
    private Event.Type type;

    public WallpaperTask(Context context, Event.Type type) {
        this.context = context;
        this.type = type;
    }

    public boolean applyWallpaper(File file) {
        final BitmapFactory.Options option = new BitmapFactory.Options();
        option.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), option);
        final WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
        final DisplayMetric displayMetric = new DisplayMetric();

        if (Util.isGlimpseEnabled(context)) {
            final Pair<Float, Float> focalPoints = BitmapUtils.findCenter(bitmap);
            bitmap = BitmapUtils.crop(bitmap,
                    focalPoints.getFirst(),
                    focalPoints.getSecond(),
                    displayMetric.getWidth(),
                    displayMetric.getHeight());
        }

        try {
            if (Build.VERSION.SDK_INT < 24) {
                wallpaperManager.setBitmap(bitmap);
            } else {
                switch (type) {
                    case WALLPAPER_LOCK:
                        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
                        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_SYSTEM);
                        break;
                    case WALLPAPER_ONLY:
                        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_SYSTEM);
                        break;
                    case LOCK_ONLY:
                        wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
                        break;
                }
            }
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }
}
