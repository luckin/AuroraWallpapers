/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.aurora.wallpapers.model.bing.BingWall;
import com.aurora.wallpapers.model.fastitems.BingItem;
import com.aurora.wallpapers.task.DailyBingWallsTask;
import com.aurora.wallpapers.utils.Log;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BingWallViewModel extends AndroidViewModel {

    private MutableLiveData<List<BingItem>> data = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public BingWallViewModel(@NonNull Application application) {
        super(application);
        fetchData();
    }

    public LiveData<List<BingItem>> getData() {
        return data;
    }

    private void fetchData() {
        disposable.add(Observable.fromCallable(() -> new DailyBingWallsTask()
                .getBingWalls(getApplication()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::dispatchItems, throwable -> Log.e(throwable.getMessage())));
    }

    private void dispatchItems(List<BingWall> wallList) {
        disposable.add(Observable.fromIterable(wallList)
                .map(BingItem::new)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wallItems -> data.setValue(wallItems), throwable -> Log.e(throwable.getMessage())));
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
        super.onCleared();
    }
}