/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.aurora.wallpapers.AuroraApplication;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.event.Event;
import com.aurora.wallpapers.model.bing.BingWall;
import com.aurora.wallpapers.task.WallpaperTask;
import com.aurora.wallpapers.task.downloader.BingDownloader;
import com.aurora.wallpapers.ui.sheet.ApplySheet;
import com.aurora.wallpapers.utils.FileUtil;
import com.aurora.wallpapers.utils.ImageUtil;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.Util;
import com.aurora.wallpapers.utils.ViewUtil;
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.loader.ImageLoader;
import com.github.piasy.biv.view.BigImageView;
import com.github.piasy.biv.view.GlideImageViewFactory;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BingActivity extends AppCompatActivity {

    @BindView(R.id.photo_view)
    BigImageView bigImageView;
    @BindView(R.id.line1)
    TextView line1;
    @BindView(R.id.line2)
    TextView line2;
    @BindView(R.id.line3)
    TextView line3;
    @BindView(R.id.img_apply)
    AppCompatImageView imgApply;
    @BindView(R.id.action_layout)
    RelativeLayout actionLayout;

    private BingWall wall;
    private Boolean pendingCrop = false;
    private Gson gson = new Gson();
    private File tempFile = null;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtil.configureActivityLayout(this);
        setContentView(R.layout.activity_bing);
        ButterKnife.bind(this);

        checkPermissions();

        disposable.add(AuroraApplication
                .getRelayBus()
                .subscribe(event -> {
                    if (event.getStringExtra().equals(wall.getHsh())) {
                        if (event.getType() == Event.Type.CROP) {
                            if (FileUtil.isWallpaperDownloaded(wall.getHsh())) {
                                File file = FileUtil.getDownloadedWallPath(this, Util.getBingFilename(wall.getHsh()));
                                if (file != null)
                                    setImageAsWallpaperPicker(file.getPath());
                                else
                                    Toast.makeText(this, getString(R.string.wallpaper_not_ready), Toast.LENGTH_SHORT).show();
                            } else {
                                if (tempFile != null) {
                                    copyTempFileToDownloadsAndApply();
                                } else {
                                    pendingCrop = true;
                                    downloadImageAndApply();
                                }
                            }
                        } else {
                            applyWallpaper(event.getType());
                        }
                    } else {
                        Log.e("Something sketchy just happened !");
                    }
                }, throwable -> Log.e(throwable.getMessage())));
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            String stringExtra = intent.getStringExtra(Constants.STRING_EXTRA);
            if (stringExtra != null) {
                wall = gson.fromJson(stringExtra, BingWall.class);
                if (wall != null)
                    populateDetails();
                else
                    finishAfterTransition();
            }
        } else
            finishAfterTransition();
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    @OnClick(R.id.img_apply)
    public void applyWallpaperSheet() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(ApplySheet.TAG) == null) {
            final ApplySheet sheet = new ApplySheet();
            final Bundle bundle = new Bundle();
            bundle.putString(Constants.STRING_EXTRA, wall.getHsh());
            sheet.setArguments(bundle);
            sheet.show(getSupportFragmentManager(), ApplySheet.TAG);
        }
    }

    private void copyTempFileToDownloadsAndApply() {
        disposable.add(Observable.fromCallable(() -> IOUtils.copyLarge(
                new FileInputStream(tempFile),
                new FileOutputStream(FileUtil.getFileFromBingWall(wall))))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    if (aLong > 0) {
                        File file = FileUtil.getDownloadedWallPath(this, Util.getBingFilename(wall.getHsh()));
                        if (file != null) {
                            notifyMediaStoreScanner(file);
                            setImageAsWallpaperPicker(file.getPath());
                        }
                    }
                }));
    }

    public void downloadImageAndApply() {
        disposable.add(Observable.fromCallable(() -> new BingDownloader(wall).getWallpaper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(file -> {
                    Toast.makeText(BingActivity.this, getString(R.string.download_completed), Toast.LENGTH_LONG).show();
                    notifyMediaStoreScanner(file);
                    if (pendingCrop) {
                        setImageAsWallpaperPicker(file.getPath());
                    }
                }, Throwable::printStackTrace));
    }

    public final void notifyMediaStoreScanner(final File file) {
        getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
    }

    private void populateDetails() {
        bigImageView.setImageViewFactory(new GlideImageViewFactory());
        bigImageView.setProgressIndicator(new ProgressPieIndicator());
        bigImageView.showImage(Uri.parse(Util.getBingUrl(wall.getUrl())));
        bigImageView.setImageLoaderCallback(new ImageLoader.Callback() {
            @Override
            public void onCacheHit(int i, File file) {

            }

            @Override
            public void onCacheMiss(int i, File file) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onProgress(int i) {

            }

            @Override
            public void onFinish() {

            }

            @Override
            public void onSuccess(File file) {
                tempFile = file;
            }

            @Override
            public void onFail(Exception e) {

            }
        });

        line1.setText(wall.getTitle());
        line2.setText(wall.getCopyright());
        line3.setText(Util.getBingDate(wall.getStartdate()));

        int textColor = Color.WHITE;
        line1.setTextColor(textColor);
        line2.setTextColor(textColor);
        line3.setTextColor(textColor);

        actionLayout.setBackground(ImageUtil.getGradientDrawable(Color.BLACK));
    }

    private void applyWallpaper(Event.Type type) {
        File file = bigImageView.getCurrentImageFile();
        if (file == null) {
            Toast.makeText(this, getString(R.string.wallpaper_not_ready), Toast.LENGTH_SHORT).show();
        } else {
            disposable.add(Observable.fromCallable(() -> new WallpaperTask(this, type)
                    .applyWallpaper(file))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success -> {
                        Toast.makeText(this, getString(success
                                        ? R.string.wallpaper_applied
                                        : R.string.wallpaper_failed),
                                Toast.LENGTH_SHORT).show();
                    }, throwable -> Log.e(throwable.getMessage())));
        }
    }

    private void setImageAsWallpaperPicker(String fileUri) {
        Intent intent = new Intent(Intent.ACTION_ATTACH_DATA);
        intent.setType("image/*");
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String extension = MimeTypeMap.getFileExtensionFromUrl(fileUri);
        String mimeType = map.getMimeTypeFromExtension(extension);
        intent.setDataAndType(Uri.parse(fileUri), mimeType);
        intent.putExtra("mimeType", mimeType);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(Intent.createChooser(intent, getString(R.string.action_set_as_crop)));
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1337);
        }
    }
}
