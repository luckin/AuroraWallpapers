/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.DisplayMetric;
import com.aurora.wallpapers.model.fastitems.BingItem;
import com.aurora.wallpapers.model.fastitems.decor.EqualSpacingItemDecoration;
import com.aurora.wallpapers.ui.activity.BingActivity;
import com.aurora.wallpapers.ui.view.ViewFlipper2;
import com.aurora.wallpapers.ui.viewmodel.BingWallViewModel;
import com.aurora.wallpapers.utils.ViewUtil;
import com.aurora.wallpapers.utils.diff.BingDiffCallback;
import com.google.gson.Gson;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BingFragment extends Fragment {

    @BindView(R.id.view_flipper)
    ViewFlipper2 viewFlipper;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private ItemAdapter<BingItem> itemAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecycler();

        BingWallViewModel viewModel = new ViewModelProvider(this).get(BingWallViewModel.class);
        viewModel.getData().observe(getViewLifecycleOwner(), this::dispatchUpdatesToAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void dispatchUpdatesToAdapter(List<BingItem> bingItems) {
        final FastAdapterDiffUtil fastAdapterDiffUtil = FastAdapterDiffUtil.INSTANCE;
        final BingDiffCallback diffCallback = new BingDiffCallback();
        final DiffUtil.DiffResult diffResult = fastAdapterDiffUtil.calculateDiff(itemAdapter, bingItems, diffCallback);
        fastAdapterDiffUtil.set(itemAdapter, diffResult);

        if (itemAdapter != null && itemAdapter.getAdapterItems().size() > 0) {
            viewFlipper.switchState(ViewFlipper2.DATA);
        } else {
            viewFlipper.switchState(ViewFlipper2.EMPTY);
        }
    }

    private void setupRecycler() {
        final FastAdapter<BingItem> fastAdapter = new FastAdapter<>();
        itemAdapter = new ItemAdapter<>();

        fastAdapter.addAdapter(0, itemAdapter);

        fastAdapter.setOnClickListener((view, iAdapter, item, position) -> {
            Intent intent = new Intent(requireContext(), BingActivity.class);
            intent.putExtra(Constants.STRING_EXTRA, new Gson().toJson(item.getWall()));
            requireActivity().startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
            return false;
        });

        DisplayMetric displayMetric = new DisplayMetric();
        int gridCount = 2;
        if (displayMetric.getWidth() > displayMetric.getHeight())
            gridCount = 3;

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(), gridCount);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16));
        recyclerView.setAdapter(fastAdapter);
    }
}
