/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.DisplayMetric;
import com.aurora.wallpapers.model.fastitems.WallItem;
import com.aurora.wallpapers.model.fastitems.decor.EqualSpacingItemDecoration;
import com.aurora.wallpapers.ui.activity.DetailsActivity;
import com.aurora.wallpapers.ui.view.ViewFlipper2;
import com.aurora.wallpapers.ui.viewmodel.CategoryViewModel;
import com.aurora.wallpapers.utils.Util;
import com.aurora.wallpapers.utils.ViewUtil;
import com.google.gson.Gson;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @BindView(R.id.view_flipper)
    ViewFlipper2 viewFlipper;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeLayout;

    private CategoryViewModel.Category subcategory;

    private CategoryViewModel viewModel;
    private FastAdapter fastAdapter;
    private ItemAdapter<WallItem> itemAdapter;
    private EndlessRecyclerOnScrollListener endlessScrollListener;
    private SharedPreferences sharedPreferences;

    public CategoryViewModel.Category getSubcategory() {
        return subcategory;
    }

    private void setSubcategory(Bundle bundle) {
        String category = bundle.getString("SUBCATEGORY");
        if (category != null)
            switch (category) {
                case "RANDOM":
                    subcategory = CategoryViewModel.Category.RANDOM;
                    break;
                case "LATEST":
                    subcategory = CategoryViewModel.Category.LATEST;
                    break;
                case "TOPLIST":
                    subcategory = CategoryViewModel.Category.TOPLIST;
                    break;
                case "TRENDING":
                    subcategory = CategoryViewModel.Category.TRENDING;
                    break;
            }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sharedPreferences = Util.getPrefs(requireContext());
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        final Bundle bundle = getArguments();
        if (bundle != null) {
            setSubcategory(bundle);
        }

        if (getSubcategory() != null) {
            setupRecycler();
            viewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
            viewModel.getData().observe(getViewLifecycleOwner(), wallItems -> {
                itemAdapter.add(wallItems);

                if (itemAdapter != null && itemAdapter.getAdapterItems().size() > 0) {
                    viewFlipper.switchState(ViewFlipper2.DATA);
                } else {
                    viewFlipper.switchState(ViewFlipper2.EMPTY);
                }

                swipeLayout.setRefreshing(false);
            });
            viewModel.fetchData(getSubcategory());

            if (subcategory == CategoryViewModel.Category.RANDOM) {
                swipeLayout.setOnRefreshListener(this::purgeAndGetData);
            } else {
                swipeLayout.setOnRefreshListener(null);
                swipeLayout.setEnabled(false);
            }
        }
    }

    @Override
    public void onPause() {
        swipeLayout.setRefreshing(false);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        try {
            sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }

    private void setupRecycler() {
        fastAdapter = new FastAdapter<>();
        itemAdapter = new ItemAdapter<>();

        fastAdapter.addAdapter(0, itemAdapter);

        fastAdapter.setOnClickListener((view, iAdapter, item, position) -> {
            if (item instanceof WallItem) {
                Intent intent = new Intent(requireContext(), DetailsActivity.class);
                intent.putExtra(Constants.STRING_EXTRA, new Gson().toJson(((WallItem) item).getWall()));
                requireActivity().startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
            }
            return false;
        });

        DisplayMetric displayMetric = new DisplayMetric();
        int gridCount = 2;
        if (displayMetric.getWidth() > displayMetric.getHeight())
            gridCount = 3;

        GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(), gridCount);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16));

        endlessScrollListener = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                viewModel.fetchData(getSubcategory());
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        recyclerView.setAdapter(fastAdapter);
    }

    private void purgeAndGetData() {
        itemAdapter.clear();
        endlessScrollListener.resetPageCount();
        viewModel.resetPage();
        viewModel.fetchData(getSubcategory());
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case Constants.PREFERENCE_FILTER:
            case Constants.PREFERENCE_FILTER_ENABLED:
                purgeAndGetData();
                break;
        }
    }
}
