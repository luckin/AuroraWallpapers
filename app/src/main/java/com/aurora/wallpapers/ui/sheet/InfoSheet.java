/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.sheet;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.GlideApp;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.model.wallhaven.Tag;
import com.aurora.wallpapers.model.wallhaven.WallInfo;
import com.aurora.wallpapers.ui.activity.SearchActivity;
import com.aurora.wallpapers.utils.ViewUtil;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

public class InfoSheet extends BaseBottomSheet {

    public static final String TAG = "INFO_SHEET";

    @BindView(R.id.txt_resolution)
    TextView txtResolution;
    @BindView(R.id.txt_ratio)
    TextView txtRatio;
    @BindView(R.id.txt_file_type)
    TextView txtFileType;
    @BindView(R.id.txt_url)
    TextView txtUrl;
    @BindView(R.id.chip_group_tags)
    ChipGroup chipGroupTags;
    @BindView(R.id.img)
    AppCompatImageView img;
    @BindView(R.id.txt_uploader)
    TextView txtUploader;
    @BindView(R.id.chip_group_advance)
    ChipGroup chipGroupAdvance;

    public InfoSheet() {
    }

    @Nullable
    @Override
    protected View onCreateContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sheet_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onContentViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onContentViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            stringExtra = bundle.getString(Constants.STRING_EXTRA);
            final WallInfo wallInfo = gson.fromJson(stringExtra, WallInfo.class);
            if (wallInfo != null)
                populateData(wallInfo);
            else
                dismissAllowingStateLoss();
        } else {
            dismissAllowingStateLoss();
        }
    }

    private void populateData(WallInfo wallinfo) {
        if (wallinfo.getUploader() != null) {
            if (wallinfo.getUploader().getAvatar() != null) {
                GlideApp
                        .with(requireContext())
                        .asBitmap()
                        .apply(new RequestOptions().circleCrop())
                        .transition(withCrossFade())
                        .load(wallinfo.getUploader().getAvatar().get_128px())
                        .into(img);
            }
            txtUploader.setText(String.valueOf(wallinfo.getUploader().getUsername()));

            final Chip uploaderChip = new Chip(requireContext());
            uploaderChip.setText(getString(R.string.search_more_uploader));
            uploaderChip.setCheckable(false);
            uploaderChip.setOnClickListener(v -> searchMoreUploaderWalls(wallinfo.getUploader().getUsername()));
            chipGroupAdvance.addView(uploaderChip);
        }

        txtResolution.setText(wallinfo.getResolution());
        txtRatio.setText(wallinfo.getRatio());
        txtFileType.setText(wallinfo.getFileType());
        txtUrl.setText(wallinfo.getPath());

        if (wallinfo.getTags() != null) {
            for (Tag tag : wallinfo.getTags()) {
                Chip chip = new Chip(requireContext());
                chip.setText(tag.name);
                chip.setCheckable(false);
                chip.setOnClickListener(v -> searchWallsWithTag(tag));
                chipGroupTags.addView(chip);
            }
        }

        final Chip similarChip = new Chip(requireContext());
        similarChip.setText(getString(R.string.search_similar));
        similarChip.setCheckable(false);
        similarChip.setOnClickListener(v -> searchSimilarWalls(wallinfo.getId()));

        chipGroupAdvance.addView(similarChip);
    }

    private void searchWallsWithTag(Tag tag) {
        Intent intent = new Intent(requireContext(), SearchActivity.class);
        intent.putExtra(Constants.STRING_EXTRA, String.valueOf(tag.getId()));
        intent.putExtra(Constants.TYPE_EXTRA, "TAG");
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }

    private void searchSimilarWalls(String wallpaperId) {
        Intent intent = new Intent(requireContext(), SearchActivity.class);
        intent.putExtra(Constants.STRING_EXTRA, wallpaperId);
        intent.putExtra(Constants.TYPE_EXTRA, "SIMILAR");
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }

    private void searchMoreUploaderWalls(String uploader) {
        Intent intent = new Intent(requireContext(), SearchActivity.class);
        intent.putExtra(Constants.STRING_EXTRA, uploader);
        intent.putExtra(Constants.TYPE_EXTRA, "UPLOADER");
        startActivity(intent, ViewUtil.getEmptyActivityBundle((AppCompatActivity) requireActivity()));
    }
}
