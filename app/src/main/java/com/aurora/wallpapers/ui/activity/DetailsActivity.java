/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.aurora.wallpapers.AuroraApplication;
import com.aurora.wallpapers.Constants;
import com.aurora.wallpapers.R;
import com.aurora.wallpapers.event.Event;
import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.model.wallhaven.WallInfo;
import com.aurora.wallpapers.task.WallpaperTask;
import com.aurora.wallpapers.task.downloader.WallHavenDownloader;
import com.aurora.wallpapers.ui.sheet.ApplySheet;
import com.aurora.wallpapers.ui.sheet.InfoSheet;
import com.aurora.wallpapers.ui.sheet.PaletteSheet;
import com.aurora.wallpapers.ui.viewmodel.WallInfoViewModel;
import com.aurora.wallpapers.utils.ColorUtil;
import com.aurora.wallpapers.utils.FileUtil;
import com.aurora.wallpapers.utils.ImageUtil;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.Util;
import com.aurora.wallpapers.utils.ViewUtil;
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.loader.ImageLoader;
import com.github.piasy.biv.view.BigImageView;
import com.github.piasy.biv.view.GlideImageViewFactory;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.photo_view)
    BigImageView bigImageView;
    @BindView(R.id.line1)
    TextView line1;
    @BindView(R.id.line2)
    TextView line2;
    @BindView(R.id.line3)
    TextView line3;
    @BindView(R.id.img_apply)
    AppCompatImageView imgApply;
    @BindView(R.id.img_info)
    AppCompatImageView imgInfo;
    @BindView(R.id.img_download)
    AppCompatImageView imgDownload;
    @BindView(R.id.img_favourite)
    AppCompatImageView imgFavourite;
    @BindView(R.id.img_palette)
    AppCompatImageView imgPalette;
    @BindView(R.id.root_layout)
    RelativeLayout rootLayout;
    @BindView(R.id.action_layout)
    LinearLayout actionLayout;

    private Wall wall;
    private WallInfo wallInfo;
    private Boolean pendingCrop = false;
    private Gson gson = new Gson();
    private File tempFile = null;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtil.configureActivityLayout(this);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        checkPermissions();

        disposable.add(AuroraApplication
                .getRelayBus()
                .subscribe(event -> {
                    if (event.getStringExtra().equals(wall.getId())) {
                        if (event.getType() == Event.Type.CROP) {
                            if (FileUtil.isWallpaperDownloaded(wall.getId())) {
                                File file = FileUtil.getDownloadedWallPath(this, wall.getId());
                                if (file != null)
                                    setImageAsWallpaperPicker(file.getPath());
                                else
                                    Toast.makeText(this, getString(R.string.wallpaper_not_ready), Toast.LENGTH_SHORT).show();
                            } else {
                                if (tempFile != null) {
                                    copyTempFileToDownloadsAndApply();
                                } else {
                                    pendingCrop = true;
                                    downloadImageAndApply();
                                }
                            }
                        } else {
                            applyWallpaper(event.getType());
                        }
                    } else {
                        Log.e("Something sketchy just happened !");
                    }
                }, throwable -> Log.e(throwable.getMessage())));
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            String stringExtra = intent.getStringExtra(Constants.STRING_EXTRA);
            if (stringExtra != null) {
                wall = gson.fromJson(stringExtra, Wall.class);
                if (wall != null)
                    populateDetails();
                else
                    finishAfterTransition();
            }
        } else
            finishAfterTransition();
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    @OnClick(R.id.img_info)
    public void showInfoSheet() {
        if (wallInfo == null) {
            Toast.makeText(this, getString(R.string.about_info_unavailable), Toast.LENGTH_SHORT).show();
        } else {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager.findFragmentByTag(InfoSheet.TAG) == null) {
                final InfoSheet sheet = new InfoSheet();
                final Bundle bundle = new Bundle();
                bundle.putString(Constants.STRING_EXTRA, gson.toJson(wallInfo));
                sheet.setArguments(bundle);
                sheet.show(getSupportFragmentManager(), InfoSheet.TAG);
            }
        }
    }

    @OnClick(R.id.img_apply)
    public void applyWallpaperSheet() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(ApplySheet.TAG) == null) {
            final ApplySheet sheet = new ApplySheet();
            final Bundle bundle = new Bundle();
            bundle.putString(Constants.STRING_EXTRA, wall.getId());
            sheet.setArguments(bundle);
            sheet.show(getSupportFragmentManager(), ApplySheet.TAG);
        }
    }

    @OnClick(R.id.img_palette)
    public void viewPaletteSheet() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(PaletteSheet.TAG) == null) {
            final PaletteSheet sheet = new PaletteSheet();
            final Bundle bundle = new Bundle();
            bundle.putString(Constants.STRING_EXTRA, gson.toJson(wall));
            sheet.setArguments(bundle);
            sheet.show(getSupportFragmentManager(), PaletteSheet.TAG);
        }
    }

    @OnClick(R.id.img_download)
    public void downloadImageAndApply() {
        disposable.add(Observable.fromCallable(() -> new WallHavenDownloader(wall).getWallpaper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(file -> {
                    Toast.makeText(this, getString(R.string.download_completed), Toast.LENGTH_LONG).show();
                    notifyMediaStoreScanner(file);
                    if (pendingCrop) {
                        setImageAsWallpaperPicker(file.getPath());
                    }
                }, Throwable::printStackTrace));
    }

    private void copyTempFileToDownloadsAndApply() {
        disposable.add(Observable.fromCallable(() -> IOUtils.copyLarge(
                new FileInputStream(tempFile),
                new FileOutputStream(FileUtil.getFileFromWallId(wall))))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    if (aLong > 0) {
                        File file = FileUtil.getDownloadedWallPath(this, wall.getId());
                        if (file != null) {
                            notifyMediaStoreScanner(file);
                            setImageAsWallpaperPicker(file.getPath());
                        }
                    }
                }));
    }

    public final void notifyMediaStoreScanner(final File file) {
        getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
    }

    private void populateDetails() {

        final WallInfoViewModel infoViewModel = new ViewModelProvider(this).get(WallInfoViewModel.class);
        infoViewModel.getData().observe(this, info -> {
            this.wallInfo = info;
        });
        infoViewModel.fetchData(wall.getId());

        applyColors();

        bigImageView.setImageViewFactory(new GlideImageViewFactory());
        bigImageView.setProgressIndicator(new ProgressPieIndicator());
        bigImageView.showImage(Uri.parse(wall.getThumbs().getOriginal()), Uri.parse(wall.getPath()));
        bigImageView.setImageLoaderCallback(new ImageLoader.Callback() {
            @Override
            public void onCacheHit(int i, File file) {

            }

            @Override
            public void onCacheMiss(int i, File file) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onProgress(int i) {

            }

            @Override
            public void onFinish() {

            }

            @Override
            public void onSuccess(File file) {
                tempFile = file;
            }

            @Override
            public void onFail(Exception e) {

            }
        });

        line1.setText(StringUtils.joinWith(" \u2022 ",
                StringUtils.capitalize(wall.getCategory()),
                StringUtils.capitalize(wall.getPurity())));
        line2.setText(Util.humanReadableByteValue(wall.getFileSize(), true));
        line3.setText(wall.getSource().isEmpty() ? StringUtils.EMPTY : wall.getSource());

        boolean isFav = Util.isFave(wall.getId());
        imgFavourite.setImageDrawable(getResources().getDrawable(isFav
                ? R.drawable.ic_fav_fill
                : R.drawable.ic_fav_line));

        imgFavourite.setOnClickListener(v -> {
            if (Util.isFave(wall.getId())) {
                AuroraApplication.getFavouritesManager().removeFromWallMap(wall);
                imgFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav_line));
            } else {
                AuroraApplication.getFavouritesManager().addToWallMap(wall);
                imgFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_fav_fill));
            }
        });
    }

    private void applyColors() {
        if (wall.getColors() != null && !wall.getColors().isEmpty()) {
            int backgroundColor = Color.parseColor(wall.getColors().get(0));
            int textColor = Color.WHITE;

            actionLayout.setBackground(ImageUtil.getGradientDrawable(backgroundColor));

            if (ColorUtil.isColorLight(backgroundColor)) {
                textColor = ColorUtil.manipulateColor(backgroundColor, 0.25f);
            }

            line1.setTextColor(textColor);
            line2.setTextColor(textColor);
            line3.setTextColor(textColor);

            imgDownload.setImageTintList(ColorStateList.valueOf(textColor));
            imgFavourite.setImageTintList(ColorStateList.valueOf(textColor));
            imgPalette.setImageTintList(ColorStateList.valueOf(textColor));
            imgInfo.setImageTintList(ColorStateList.valueOf(textColor));


            imgApply.setImageTintList(ColorStateList.valueOf(ColorUtil.isColorLight(textColor) ? Color.GRAY : Color.WHITE));
            imgApply.setBackgroundTintList(ColorStateList.valueOf(textColor));
        }
    }

    private void applyWallpaper(Event.Type type) {
        File file = bigImageView.getCurrentImageFile();
        if (file == null) {
            Toast.makeText(this, getString(R.string.wallpaper_not_ready), Toast.LENGTH_SHORT).show();
        } else {
            disposable.add(Observable.fromCallable(() -> new WallpaperTask(this, type)
                    .applyWallpaper(file))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success -> {
                        Toast.makeText(this, getString(success
                                        ? R.string.wallpaper_applied
                                        : R.string.wallpaper_failed),
                                Toast.LENGTH_SHORT).show();
                    }, throwable -> Log.e(throwable.getMessage())));
        }
    }

    private void setImageAsWallpaperPicker(String fileUri) {
        Intent intent = new Intent(Intent.ACTION_ATTACH_DATA);
        intent.setType("image/*");
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String extension = MimeTypeMap.getFileExtensionFromUrl(fileUri);
        if (StringUtils.isEmpty(extension) || extension.length() < 2) {
            extension = ".jpg";
        }
        String mimeType = map.getMimeTypeFromExtension(extension);
        intent.setDataAndType(Uri.parse(fileUri), mimeType);
        intent.putExtra("mimeType", mimeType);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(Intent.createChooser(intent, getString(R.string.action_set_as_crop)));
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1337);
        }
    }
}
