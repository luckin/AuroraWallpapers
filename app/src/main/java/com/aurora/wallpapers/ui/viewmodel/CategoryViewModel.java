/*
 *  Aurora Wallpapers
 *  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
 *
 *  Aurora Wallpapers is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Aurora Wallpapers is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.wallpapers.ui.viewmodel;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.aurora.wallpapers.model.Filter;
import com.aurora.wallpapers.model.Wall;
import com.aurora.wallpapers.model.fastitems.WallItem;
import com.aurora.wallpapers.model.wallhaven.Search;
import com.aurora.wallpapers.retro.RetroClient;
import com.aurora.wallpapers.retro.WallHavenService;
import com.aurora.wallpapers.utils.Log;
import com.aurora.wallpapers.utils.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryViewModel extends AndroidViewModel {

    private MutableLiveData<List<WallItem>> data = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    private int currentPage = 1;
    private String seed = null;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
    }

    public void resetPage() {
        currentPage = 1;
    }

    public LiveData<List<WallItem>> getData() {
        return data;
    }

    public void fetchData(Category category) {
        WallHavenService service = RetroClient.getInstance().create(WallHavenService.class);
        Call<Search> call = null;
        Filter filter = Filter.getSavedFilter(getApplication());

        if (Filter.isEnabled(getApplication()) && filter != null) {
            call = service.getWalls(
                    Util.getWallHavenAPIKey(getApplication()),
                    category.getCategory(),
                    filter.getAllRatios(),
                    filter.getAllResolutions(),
                    filter.getPurity(),
                    filter.getCategory(),
                    currentPage++,
                    category == Category.RANDOM ? seed : null,
                    category == Category.TOPLIST ? "1w" : null);
        } else {
            call = service.getWalls(
                    Util.getWallHavenAPIKey(getApplication()),
                    category.getCategory(),
                    null,
                    null,
                    "100",
                    "101",
                    currentPage++,
                    category == Category.RANDOM ? seed : null,
                    category == Category.TOPLIST ? "1w" : null);
        }

        call.enqueue(new Callback<Search>() {
            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                if (response.body() != null) {
                    final Search search = response.body();
                    seed = search.getMeta().getSeed();
                    dispatchItems(search.getData());
                } else if (response.code() == 401) {
                    Toast.makeText(getApplication(), "Invalid API Key", Toast.LENGTH_SHORT).show();
                    dispatchItems(new ArrayList<>());
                } else
                    dispatchItems(new ArrayList<>());
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void dispatchItems(List<Wall> wallList) {
        disposable.add(Observable.fromIterable(wallList)
                .map(WallItem::new)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wallItems -> data.setValue(wallItems),
                        throwable -> Log.e(throwable.getMessage())));
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
        super.onCleared();
    }

    public enum Category {
        TOPLIST("toplist"),
        LATEST("date_added"),
        RANDOM("random"),
        RELEVANCE("relevance"),
        TRENDING("views");

        private String category;

        Category(String category) {
            this.category = category;
        }

        public String getCategory() {
            return category;
        }
    }
}