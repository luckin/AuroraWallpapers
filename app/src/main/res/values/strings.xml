<!--
  ~  Aurora Wallpapers
  ~  Copyright (C) 2020, Rahul Kumar Patel <auroraoss.dev@gmail.com>
  ~
  ~  Aurora Wallpapers is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 2 of the License, or
  ~  (at your option) any later version.
  ~
  ~  Aurora Wallpapers is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with Aurora Wallpapers.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <string name="app_name">"Wallpapers"</string>
    <string name="app_name_alt">"Aurora Wallpapers"</string>
    <string name="app_made_in">"Made with ❤ in India"</string>
    <string name="app_desc">"An un-official WallHaven client"</string>

    <string name="about_info">"Info"</string>
    <string name="about_palette">"Palette"</string>
    <string name="about_resolution">"Resolution"</string>
    <string name="about_ratio">"Ratio"</string>
    <string name="about_file_type">"File Type"</string>
    <string name="about_tags">"Tags"</string>
    <string name="about_advance">"Advance Search"</string>
    <string name="about_info_unavailable">"Info unavailable, fetching !"</string>
    <string name="about_paypal">"Paypal"</string>
    <string name="about_paypal_summary">"Donate via PayPal"</string>
    <string name="about_gitlab">"GitLab"</string>
    <string name="about_gitlab_summary">"Get the Aurora Walls source code from Aurora OSS on GitLab."</string>
    <string name="about_telegram">"Telegram"</string>
    <string name="about_telegram_summary">"Join the Aurora Support group for discussions or suggestions."</string>
    <string name="about_libera">"Libera Pay"</string>
    <string name="about_libera_summary">"Become patron on Libera Pay"</string>
    <string name="about_bitcoin_btc">"Bitcoin"</string>
    <string name="about_bitcoin_btc_summary">"Donate via Bitcoin (BTC)"</string>
    <string name="about_bitcoin_bch">"Bitcoin Cash"</string>
    <string name="about_bitcoin_bch_summary">"Donate via Bitcoin Cash (BCH)"</string>
    <string name="about_bitcoin_eth">"Ethereum"</string>
    <string name="about_bitcoin_eth_summary">"Donate via Ethereum (ETH)"</string>
    <string name="about_bhim">"BHIM - UPI"</string>
    <string name="about_bhim_summary">"Donate via UPI"</string>

    <string name="action_apply">"Apply"</string>
    <string name="action_close">"Close"</string>
    <string name="action_copied">"Copied to clipboard"</string>
    <string name="action_clear">"Clear"</string>
    <string name="action_filter">"Filters"</string>
    <string name="action_filter_enable">"Enable filters"</string>
    <string name="action_ignore">"Ignore"</string>
    <string name="action_loading">"Loading"</string>
    <string name="action_set_as_both">"Set as both"</string>
    <string name="action_set_as_crop">"Crop and apply"</string>
    <string name="action_set_as_lock_screen">"Set as lock screen"</string>
    <string name="action_set_as_wallpaper">"Set as wallpaper"</string>
    <string name="action_update">"Update now"</string>
    <string name="action_wallpaper_clear">"Wallpapers cleared to default"</string>
    <string name="action_wallpaper_clear_failed">"Failed to reset wallpapers"</string>

    <string name="favourite_not_found">"No favourites"</string>

    <string name="search_more_uploader">"More by uploader"</string>
    <string name="search_not_found">"No wallpapers found"</string>
    <string name="search_results">"Results for"</string>
    <string name="search_similar">"Search similar"</string>

    <string name="download_completed">"Download completed"</string>

    <string name="wallpaper_applied">"Wallpaper applied"</string>
    <string name="wallpaper_failed">"Failed to apply wallpaper"</string>
    <string name="wallpaper_not_ready">"Wallpaper not ready, wait a moment !"</string>

    <string name="filter_category">"Category"</string>
    <string name="filter_category_anime">"Anime"</string>
    <string name="filter_category_general">"General"</string>
    <string name="filter_category_people">"People"</string>
    <string name="filter_purity">"Purity"</string>
    <string name="filter_purity_nsfw">"NSFW"</string>
    <string name="filter_purity_sfw">"SFW"</string>
    <string name="filter_purity_sketchy">"Sketchy"</string>
    <string name="filter_ratios">"Ratios"</string>
    <string name="filter_resolutions">"Resolutions"</string>

    <string name="title_home">"Home"</string>
    <string name="title_dashboard">"Dashboard"</string>
    <string name="title_credits">"Credits"</string>
    <string name="title_links">"Links"</string>
    <string name="title_latest">"Latest"</string>
    <string name="title_trending">"Trending"</string>
    <string name="title_top">"Toplist"</string>
    <string name="title_random">"Random"</string>
    <string name="title_search">"Search Wallpapers"</string>
    <string name="title_bing">"Bing"</string>

    <string name="update_title">"Update"</string>
    <string name="update_desc">"New app update available"</string>
    <string name="update_changes">"Changelog"</string>
    <string name="update_failed">"Update failed"</string>
    <string name="update_unavailable">"No new version available"</string>

    <string name="menu_about">"About"</string>
    <string name="menu_bing">"Bing Daily"</string>
    <string name="menu_favourite">"Favourite"</string>
    <string name="menu_settings">"Settings"</string>

    <string name="notification_channel_updates">"Update notification"</string>

    <string name="preferences_ui_header">"User Interface"</string>
    <string name="preferences_auto_wallpaper_header">"Auto Wallpaper"</string>
    <string name="preferences_advance_header">"Advance"</string>
    <string name="preferences_theme_title">"Select theme"</string>
    <string name="preferences_auto_wallpaper">"Auto wallpaper service"</string>
    <string name="preferences_auto_wallpaper_interval">"Interval"</string>
    <string name="preferences_auto_wallpaper_target">"Target"</string>
    <string name="preferences_auto_wallpaper_source">"Source"</string>
    <string name="preferences_auto_wallpaper_summary">"Automatically change wallpaper from defined source."</string>
    <string name="preferences_bing_custom">"Custom Bing wallpaper region"</string>
    <string name="preferences_bing_custom_summary">"You can choose a custom Bing wallpaper region."</string>
    <string name="preferences_bing_custom_region">"Select Bing wallpaper region"</string>
    <string name="preferences_wallpaper_clear">"Clear wallpaper"</string>
    <string name="preferences_wallpaper_clear_summary">"Reset all wallpaper to the factory default."</string>
    <string name="preferences_wallhaven_api">"WallHaven API key"</string>
    <string name="preferences_wallhaven_api_summary">"Required to enable NSFW content."</string>
    <string name="preferences_wallpaper_glimpse">"Enable Glimpse (Experimental)"</string>
    <string name="preferences_wallpaper_glimpse_summary">"Uses Glimpse\'s eye to catch the right spot for wallpapers."</string>
</resources>
